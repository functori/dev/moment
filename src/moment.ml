open Ezjs_min

let cs : _ constr = Unsafe.global##.moment
let utc_cs : _ constr = Unsafe.global##.moment##.utc

type obj = {
  years: int option;
  months: int option;
  days: int option;
  hours: int option;
  minutes: int option;
  seconds: int option;
  milliseconds: int option;
} [@@deriving jsoo]

class type duration = object
end

let duration_cs : (obj_jsoo t -> duration t) constr = Unsafe.global##.moment##.duration

type creation = {
  input: Unsafe.any;
  format: string option;
  locale: Unsafe.any;
  isUTC: bool;
  strict: bool option;
} [@@deriving jsoo]

type calendar_options = {
  same_day: string option;
  next_day: string option;
  next_week: string option;
  last_day: string option;
  last_week: string option;
  same_else: string option;
} [@@deriving jsoo {camel}]

class type moment = object
  method isValid: bool t meth
  method invalidAt: int meth
  method creationData: creation_jsoo t meth
  method millisecond: int meth
  method millisecond_set: int -> moment t meth
  method second: int meth
  method second_set: int -> moment t meth
  method minute: int meth
  method minute_set: int -> moment t meth
  method hour: int meth
  method hour_set: int -> moment t meth
  method date: int meth
  method date_set: int -> moment t meth
  method day: int meth
  method day_set: int -> moment t meth
  method weekday: int meth
  method weekday_set: int -> moment t meth
  method isoWeekday: int meth
  method isoWeekday_set: int -> moment t meth
  method dayOfYear: int meth
  method dayOfYear_set: int -> moment t meth
  method week: int meth
  method week_set: int -> moment t meth
  method isoWeek: int meth
  method isoWeek_set: int -> moment t meth
  method month: int meth
  method month_set: int -> moment t meth
  method quarter: int meth
  method quarter_set: int -> moment t meth
  method year: int meth
  method year_set: int -> moment t meth
  method weekYear: int meth
  method weekYear_set: int -> moment t meth
  method isoWeekYear: int meth
  method isoWeekYear_set: int -> moment t meth
  method weeksInYear: int meth
  method isoWeeksInYear: int meth
  method get: js_string t -> int meth
  method set: js_string t -> int -> moment t meth
  method set_obj: obj_jsoo t -> moment t meth
  method max: moment t js_array t -> moment t meth
  method min: moment t js_array t -> moment t meth
  method add: int -> js_string t -> moment t meth
  method add_duration: duration t -> moment t meth
  method add_obj: obj_jsoo t -> moment t meth
  method subtract: int -> js_string t -> moment t meth
  method subtract_duration: duration t -> moment t meth
  method subtract_obj: obj_jsoo t -> moment t meth
  method startOf: js_string t -> moment t meth
  method endOf: js_string t -> moment t meth
  method local: moment t meth
  method utc: moment t meth
  method utcOffset: int meth
  method utcOffset_set: int -> moment t meth
  method format: js_string t optdef -> js_string t meth
  method fromNow: bool t optdef -> js_string t meth
  method from: moment t -> bool t optdef -> js_string t meth
  method toNow: bool t optdef -> js_string t meth
  method to_: moment t -> bool t optdef -> js_string t meth
  method calendar: moment t opt -> calendar_options_jsoo t optdef -> js_string t meth
  method diff: moment t -> js_string t optdef -> bool t optdef -> number t meth
  method valueOf: number t meth
  method unix: number t meth
  method daysInMonth: int meth
  method toDate: date t meth
  method toArray: int js_array t meth
  method toJSON: _ t meth
  method toISOString: js_string t meth
  method toObject: obj_jsoo t meth
  method toString: js_string t meth
  method inspect: js_string t meth
  method isBefore: moment t -> bool t meth
  method isSame: moment t -> bool t meth
  method isAfter: moment t -> bool t meth
  method isSameOrBefore: moment t -> bool t meth
  method isSameOrAfter: moment t -> bool t meth
  method isBetween: moment t -> moment t -> bool t meth
  method isDST: bool t meth
  method isLeapYear: bool t meth
  method isMoment: bool t meth
  method isDate: bool t meth
  method locale: js_string t js_array t -> moment t meth
  method locale_reset: bool t -> moment t meth
end

type long_date_options = {
  _LT: string option;
  _LTS: string option;
  _L: string option;
  _LL: string option;
  _LLL: string option;
  _LLLL: string option;
} [@@deriving jsoo]

type relative_time_options = {
  future: string option;
  past: string option;
  s: string option;
  m: string option;
  mm: string option;
  h: string option;
  hh: string option;
  d: string option;
  dd: string option;
  _M: string option;
  _MM: string option;
  y: string option;
  yy: string option;
} [@@deriving jsoo]

type locale_options = {
  months_: string list option;
  months_short: string list option;
  months_parse_exact: bool option;
  weekdays: string list option;
  weekdays_short: string list option;
  weekdays_min: string list option;
  weekdays_parse_exact: bool option;
  long_date_format: long_date_options option;
  calendar: calendar_options option;
  relative_time: relative_time_options option;
} [@@deriving jsoo {camel}]

let now ?(utc=false) () : moment t =
  let cs = if utc then utc_cs else cs in
  new%js cs

let parse ?fmt ?locale ?(strict=false) ?(utc=false)  s : moment t =
  let cs = if utc then utc_cs else cs in
  match fmt with
  | None -> new%js cs (string s)
  | Some f -> match locale, strict with
    | None, false -> new%js cs (string s) (of_listf string f)
    | Some lc, false -> new%js cs (string s) (of_listf string f) (string lc)
    | None, true -> new%js cs (string s) (of_listf string f) _true
    | Some lc, true -> new%js cs (string s) (of_listf string f) (string lc) _true

let obj ?y ?mo ?d ?h ?m ?s ?ms () : obj = {
  years=y; months=mo; days=d; hours=h; minutes=m; seconds=s; milliseconds=ms}

let make ?y ?mo ?d ?h ?m ?s ?ms ?(utc=false) () : moment t =
  let cs = if utc then utc_cs else cs in
  new%js cs (obj_to_jsoo (obj ?y ?mo ?d ?h ?m ?s ?ms ()))

let of_int ?(utc=false) (i: int) : moment t =
  let cs = if utc then utc_cs else cs in
  new%js cs i
let of_float ?(utc=false) f : moment t =
  let cs = if utc then utc_cs else cs in
  new%js cs (number_of_float f)
let of_date ?(utc=false) (d: date t) : moment t =
  let cs = if utc then utc_cs else cs in
  new%js cs d

let duration o =
  new%js duration_cs (obj_to_jsoo o)

let is_valid (m: moment t) = to_bool m##isValid
let invalid_at (m: moment t) = m##invalidAt
let inputs (m: moment t) = creation_of_jsoo m##creationData
let millisecond (m: moment t) = m##millisecond
let set_millisecond (m: moment t) i = m##millisecond_set i
let second (m: moment t) = m##second
let set_second (m: moment t) i = m##second_set i
let minute (m: moment t) = m##minute
let set_minute (m: moment t) i = m##minute_set i
let hour (m: moment t) = m##hour
let set_hour (m: moment t) i = m##hour_set i
let date (m: moment t) = m##date
let set_date (m: moment t) i = m##date_set i
let day (m: moment t) = m##day
let set_day (m: moment t) i = m##day_set i
let weekday ?(iso=false) (m: moment t) = if iso then m##isoWeekday else m##weekday
let set_weekday ?(iso=false) (m: moment t) i = if iso then m##isoWeekday_set i else m##weekday_set i
let day_of_year (m: moment t) = m##dayOfYear
let set_day_of_year (m: moment t) d = m##dayOfYear_set d
let week ?(iso=false) (m: moment t) = if iso then m##isoWeek else m##week
let set_week ?(iso=false) (m: moment t) w = if iso then m##isoWeek_set w else m##week_set w
let month (m: moment t) = m##month
let set_month (m: moment t) mo = m##month_set mo
let quarter (m: moment t) = m##quarter
let set_quarter (m: moment t) q = m##quarter_set q
let year (m: moment t) = m##year
let set_year (m: moment t) y = m##year_set y
let week_year ?(iso=false) (m: moment t) = if iso then m##isoWeekYear else m##weekYear
let set_week_year ?(iso=false) (m: moment t) w = if iso then m##isoWeekYear_set w else m##weekYear_set w
let weeks_in_year ?(iso=false) m = if iso then m##isoWeeksInYear else m##weeksInYear
let get (m: moment t) s = m##get (string s)
let set (m: moment t) o = m##set_obj (obj_to_jsoo o)
let max (m1: moment t) (m2: moment t) = m1##max (of_list [m2])
let min (m1: moment t) (m2: moment t) = m1##min (of_list [m2])
let add (m: moment t) o = m##add_obj (obj_to_jsoo o)
let subtract (m: moment t) o = m##subtract_obj (obj_to_jsoo o)
let start_of (m: moment t) s = m##startOf (string s)
let end_of (m: moment t) s = m##endOf (string s)
let local (m: moment t) = m##local
let utc (m: moment t) = m##utc
let offset (m: moment t) = m##utcOffset
let set_offset (m: moment t) i = m##utcOffset_set i
let format ?fmt (m: moment t) = to_string @@ m##format (optdef string fmt)
let from_now ?ago (m: moment t) = to_string @@ m##fromNow (optdef bool ago)
let from ?ago ~ori (m: moment t) = to_string @@ m##from ori (optdef bool ago)
let to_now ?in_ (m: moment t) = to_string @@ m##toNow (optdef bool in_)
let to_ ?in_ ~ori (m: moment t) = to_string @@ m##to_ ori (optdef bool in_)
let calendar ?ori ?fmt (m: moment t) = to_string @@ m##calendar (Opt.option ori) (optdef calendar_options_to_jsoo fmt)
let diff ?period ?float (m:moment t) m2 = float_of_number @@
  m##diff m2 (optdef string period) (optdef bool float)
let value (m: moment t) = float_of_number m##valueOf
let unix (m: moment t) = float_of_number m##unix
let days_in_month (m: moment t) = m##daysInMonth
let to_date (m: moment t) = m##toDate
let to_array (m: moment t) = m##toArray
let to_json (m: moment t) = m##toJSON
let to_iso (m: moment t) = m##toISOString
let to_string (m: moment t) = m##toString
let inspect (m: moment t) = m##inspect
let is_before (m: moment t) m2 = to_bool @@ m##isBefore m2
let is_same (m: moment t) m2 = to_bool @@ m##isSame m2
let is_after (m: moment t) m2 = to_bool @@ m##isAfter m2
let is_same_or_before (m: moment t) m2 = to_bool @@ m##isSameOrBefore m2
let is_same_or_after (m: moment t) m2 = to_bool @@ m##isSameOrAfter m2
let is_between (m: moment t) m1 m2 = to_bool @@ m##isBetween m1 m2
let compare m1 m2 =
  if is_same m1 m2 then 0
  else if is_before m1 m2 then -1
  else 1
let is_dst (m: moment t) = to_bool m##isDST
let is_leap_year (m: moment t) = to_bool m##isLeapYear
let is_moment (m: moment t) = to_bool m##isMoment
let is_date (m: moment t) = to_bool m##isDate
let locale ?loc (m: moment t) = match loc with
  | None -> m##locale_reset _false
  | Some a -> m##locale (of_listf string a)
let set_locale ?options l : unit =
  match options, l with
  | Some o, s :: _ -> Unsafe.global##.moment##locale (string s) o
  | None, _ -> Unsafe.global##.moment##locale (of_listf string l)
  | _ -> assert false
