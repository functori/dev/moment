all: build
build:
	@dune build src
clean:
	@dune clean
dev:
	@dune build --profile release
	@cp -f _build/default/test/test.bc.js test/test.js
