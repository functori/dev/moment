open Ezjs_min
open Moment

let () =
  set_locale ["fr"];
  let x = now ~utc:true () in
  let y = make ~y:2000 () in
  let d = diff x y in
  js_log d
